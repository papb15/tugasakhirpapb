package com.tugasakhirpapb.ruangbelajar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tugasakhirpapb.ruangbelajar.model.MataKuliah;

public class TambahMatkulActivity extends AppCompatActivity {

    private EditText namaInput, dosenInput;
    private Button tambahButton;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBMatkulRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_matkul);

        namaInput = findViewById(R.id.JudulInput);
        dosenInput = findViewById(R.id.DeadlineInput);
        tambahButton = findViewById(R.id.TambahButton);

        tambahButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TambahKelas();
            }
        });

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL MATAKULIAH
        mDBMatkulRef = mDatabase.getReference("MataKuliah");


    }

    private void TambahKelas() {
        String nama = namaInput.getText().toString();
        String dosen = dosenInput.getText().toString();

        if (nama == null) {
            Toast.makeText(TambahMatkulActivity.this, "Nama Kelas Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (dosen == null) {
            Toast.makeText(TambahMatkulActivity.this, "Nama Dosen Pengampu Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            MataKuliah newMatkul = new MataKuliah();
            newMatkul.setNamaMataKuliah(nama);
            newMatkul.setDosenPengampu(dosen);
            mDBMatkulRef.push().setValue(newMatkul);
            Toast.makeText(TambahMatkulActivity.this, "Kelas Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}