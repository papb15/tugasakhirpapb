package com.tugasakhirpapb.ruangbelajar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tugasakhirpapb.ruangbelajar.controller.MataKuliahAdapter;
import com.tugasakhirpapb.ruangbelajar.model.MataKuliah;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView userEmailTextView;
    private Button logoutButton, tambahButton;

    private RecyclerView recyclerViewMatkul;
    private MataKuliahAdapter mataKuliahAdapter;

    private FirebaseAuth mAuth;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBMatkulRef;

    private ArrayList<MataKuliah> mataKuliahList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MENCARI VIEW
        userEmailTextView = findViewById(R.id.UserEmailTextView);
        logoutButton = findViewById(R.id.LogoutButton);
        tambahButton = findViewById(R.id.TambahKelasButton);
        recyclerViewMatkul = findViewById(R.id.RecycleViewMataKuliah);

        mataKuliahAdapter = new MataKuliahAdapter(MainActivity.this, mataKuliahList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);

        recyclerViewMatkul.setLayoutManager(layoutManager);
        recyclerViewMatkul.setAdapter(mataKuliahAdapter);

        //KLIK TOMBOL LOGOUT
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestLogout();
            }
        });

        tambahButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tambahActivity = new Intent(MainActivity.this, TambahMatkulActivity.class);
                startActivity(tambahActivity);
            }
        });

        //INSTANSIASI FIREBASE AUTH
        mAuth = FirebaseAuth.getInstance();

        //INSTANSIASI FIREBASE DATABASE
        mDatabase = FirebaseDatabase.getInstance("https://ruang-belajar-6570f-default-rtdb.asia-southeast1.firebasedatabase.app/");

        //REFERENSI DATABASE KE TABEL MATAKULIAH
        mDBMatkulRef = mDatabase.getReference("MataKuliah");

        mDBMatkulRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                mataKuliahList.clear();
                for (DataSnapshot data : snapshot.getChildren()) {
                    MataKuliah matkul = data.getValue(MataKuliah.class);
                    matkul.setId(data.getKey());
                    mataKuliahList.add(matkul);
                }
                mataKuliahAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "Kesalahan Memuat Data", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        //MEMERIKSA USER
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
            //KE HALAMAN LOGIN SAAT TIDAK ADA USER YANG LOGIN
            GoToLoginActivity();
        } else {
            userEmailTextView.setText(currentUser.getEmail());
        }
    }

    private void GoToLoginActivity() {
        //MENGUBAH ACTIVITY KE ACTIVITY LOGIN
        Intent loginActivity = new Intent(MainActivity.this, LoginActivity.class);
        loginActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(loginActivity);
        this.finish();
    }

    private void RequestLogout() {
        //MELAKUKAN LOGOUT DAN KE HALAMAN LOGIN
        mAuth.signOut();
        GoToLoginActivity();
    }
}